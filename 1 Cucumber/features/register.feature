Feature: register into losestudiantes
    As an user I want to register myself within losestudiantes website in order to rate teachers

Scenario Outline: Register failed with a login that already exists

  Given I go to losestudiantes home screen
    When I open the login screen
    And I fill with <name>, <lastname>, <email>, <university>, <department>, <accept> and <password>
    And I try to register
    Then I expect to see <error>

    Examples:
      | name			| lastname			| email					| university			| department| accept	| password	| error								|
      | Prueba Nombre	| Apellido Prueba	| prueba@prueba.com		| universidad-nacional	| 15		| true		| 12345678	| Ya existe un usuario registrado	|

	  
Scenario Outline: Register failed with bad inputs

  Given I go to losestudiantes home screen
    When I open the login screen
    And I fill with <name>, <lastname>, <email>, <university>, <department>, <accept> and <password>
    And I try to register
    Then I should to see <error>

    Examples:
      | name			| lastname			| email					| university			| department| accept	| password	| error									  			|
	  | 				| 					| 						| 						| 			| 			| 			| Ingresa tu correo									|
	  | Prueba Nombre	| Apellido Prueba	| 						| universidad-nacional	| 15		| true		| 12345678	| Ingresa tu correo									|
	  | Prueba Nombre	| Apellido Prueba	| p						| universidad-nacional	| 15		| true		| 12345678	| Ingresa un correo valido							|
	  | Prueba Nombre	| Apellido Prueba	| prueba@prueba.com		| universidad-nacional	| 15		| true		| 			| Ingresa una contraseña							|
	  | Prueba Nombre	| Apellido Prueba	| prueba@prueba.com		| universidad-nacional	| 15		| true		| 12		| La contraseña debe ser al menos de 8 caracteres	|
      | Prueba Nombre	| Apellido Prueba	| prueba@prueba.com		| universidad-nacional	| 15		| false		| 12345678	| Debes aceptar los términos y condiciones			|
	  
Scenario Outline: Register successed with good inputs

  Given I go to losestudiantes home screen
    When I open the login screen
    And I fill with <name>, <lastname>, <email>, <university>, <department>, <accept> and <password>
    And I try to register
    Then I expect to see <success>

    Examples:
      | name			| lastname			| email					| university			| department| accept	| password	| success			|
      | Prueba Nombre	| Apellido Prueba	| prueba2@prueba.com	| universidad-nacional	| 15		| true		| 12345678	| Registro exitoso!	|