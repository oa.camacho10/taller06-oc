//Complete siguiendo las instrucciones del taller
var {defineSupportCode} = require('cucumber');
var {expect} = require('chai');

defineSupportCode(({Given, When, Then}) => {
  Given('I go to losestudiantes home screen', () => {
    browser.url('/');
    if(browser.isVisible('button=Cerrar')) {
      browser.click('button=Cerrar');
    }
  });

  When('I open the login screen', () => {
    browser.waitForVisible('button=Ingresar', 5000);
    browser.click('button=Ingresar');
  });

  When(/^I fill with (.*), (.*), (.*), (.*), (.*), (.*) and (.*)$/ , (name, lastname, email, university, department, accept, password ) => {
	
	var cajaSignUp = browser.element('.cajaSignUp');
        
	var nombreInput = cajaSignUp.element('input[name="nombre"]');
	nombreInput.click();
    nombreInput.keys(name);	
	
	var apellidoInput = cajaSignUp.element('input[name="apellido"]');
	apellidoInput.click();
    apellidoInput.keys(lastname);
	
	var correoInput = cajaSignUp.element('input[name="correo"]');
	correoInput.click();
    correoInput.keys(email);
	
	var idDepartamentoSelect = cajaSignUp.element('select[name="idDepartamento"]');
	idDepartamentoSelect.click();
    idDepartamentoSelect.selectByValue(department);
	
	var passwordInput = cajaSignUp.element('input[name="password"]');
	passwordInput.click();
    passwordInput.keys(password);
	
	var aceptaInput = cajaSignUp.element('input[name="acepta"]');
	if(!aceptaInput.isSelected() && accept == 'true'){
		aceptaInput.click();
	}
	
  });

  When('I try to register', () => {
    var cajaSignUp = browser.element('.cajaSignUp');
    cajaSignUp.element('button=Registrarse').click()
  });
  
  Then('I expect to see {string}', error => {
    browser.waitForVisible('.sweet-alert', 5000);
	var alertText = browser.element('.sweet-alert').getText();
	expect(alertText).to.include(error);
  });
  
  Then('I should to see {string}', error => {
	var cajaSignUp = browser.element('.cajaSignUp');
    cajaSignUp.waitForVisible('.aviso.alert.alert-danger', 5000);
	var alertText = cajaSignUp.element('.aviso.alert.alert-danger').getText();
	expect(alertText).to.include(error);
  });
  
});